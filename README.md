# About Me
I'm Viola 🎻, tHiS iS mY gItLaB pRoFiLe.

- 🌍  Aachen, Germany
- 🎓  Alumni of Computer Science at RWTH Aachen
- 💻 Interests: Creative work, motorcycling and gaming

## Projects

### [Bachelor's Thesis](https://github.com/ViolaValery/darkpatterns_extension)
- Description: My bachelor's thesis project focused on implementing countermeasures against [deceptive patterns](https://www.deceptive.design/) directly in the browser.
- Tool: A Chrome Extension framework.
- Goal: Establishing a foundation for integrating a detection unit and a converter unit into my framework. The detection unit identifies the dark patterns, while the converter determines the appropriate countermeasures to apply.


## Contact Me
You can reach out to me via:
- 📧 Email: valery.graf@rwth-aachen.de


